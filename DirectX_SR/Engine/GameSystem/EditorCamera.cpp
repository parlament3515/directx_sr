#include "pch.h"
#include "EditorCamera.h"
#include "Transform.h"
#include "Camera.h"
#include "ManagerDef.h"

namespace ce
{
	EditorCamera::EditorCamera(HWND hWnd) noexcept :
		_ptPrevMousePos(),
		_hWnd(hWnd)
	{
		_eLayer = GameObjectLayer::OBJECT;
	}

	bool EditorCamera::Init(void) noexcept
	{
		AddComponent(new Camera(this, D3D9DEVICE->GetDevice()));

		GetCursorPos(&_ptPrevMousePos);
		_pTransform = GetTransform();
		return true;
	}

	void EditorCamera::Update(float fElapsedTime) noexcept
	{
		if (INPUT->GetKeyStay('A') || INPUT->GetKeyStay('a'))
		{
			_pTransform->Translate(-10 * fElapsedTime, 0, 0);
		}

		if (INPUT->GetKeyStay('D') || INPUT->GetKeyStay('d'))
		{
			_pTransform->Translate(+10 * fElapsedTime, 0, 0);
		}

		if (INPUT->GetKeyStay('W') || INPUT->GetKeyStay('w'))
		{
			_pTransform->Translate(0, 0, +10 * fElapsedTime);
		}

		if (INPUT->GetKeyStay('S') || INPUT->GetKeyStay('s'))
		{
			_pTransform->Translate(0, 0, -10 * fElapsedTime);
		}

		if (INPUT->GetKeyDown(VK_TAB))
		{
			if (_bWireFrame == false)
				D3D9DEVICE->GetDevice()->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
			else
				D3D9DEVICE->GetDevice()->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

			_bWireFrame = !_bWireFrame;
		}

		if (INPUT->GetKeyDown(VK_RBUTTON))
		{
			_bMBRightDown = true;
			GetCursorPos(&_ptPrevMousePos);
		}

		else if (INPUT->GetKeyStay(VK_RBUTTON))
		{
			POINT pt;
			GetCursorPos(&pt);

			int dx = pt.x - _ptPrevMousePos.x;
			int dy = pt.y - _ptPrevMousePos.y;

			_pTransform->Rotate(dy * fElapsedTime * 0.5f, dx * fElapsedTime * 0.5f, 0);

			_ptPrevMousePos = pt;
		}
	}

	void EditorCamera::Render(void) noexcept
	{

	}

	void EditorCamera::Release(void) noexcept
	{

	}
}