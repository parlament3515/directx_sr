#pragma once
#include "Component.h"

namespace ce
{
	class GameObject;

	class Light : public Component
	{
	public:		explicit		Light(void) = delete;

	};
}
