#pragma once
#include "Texture.h"

namespace ce
{
	class Material
	{
	public:		explicit				Material(void) noexcept;
	public:		virtual					~Material(void) noexcept { __noop; }

	public:		void					SetMainTexture(Texture* texture) noexcept;
	public:		void					SetColor(D3DCOLOR c) noexcept { _color = c; }

	public:		const Texture*			GetMainTexture(void) const noexcept { return _textures.front();}
	public:		D3DCOLOR				GetColor(void) const noexcept { return _color; }

	public:		void					SetTextures(Texture** ppTexture, uint16 count);
	public:		void					SetTexture(Texture* pTexture) noexcept;
	public:		Texture*				GetTexture(uint16 index);
	public:		const Texture::TList&	GetTextures(void) const noexcept { return _textures; }

	private:	Texture::TList			_textures;
	private:	D3DCOLOR				_color;
	};
}
