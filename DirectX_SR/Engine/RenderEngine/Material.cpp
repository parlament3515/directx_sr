#include "pch.h"
#include "Material.h"
#include "Assertion.h"

namespace ce
{
	Material::Material(void) noexcept :
		_color(D3DCOLOR_ARGB(255, 255, 255, 255))
	{
	}

	void Material::SetMainTexture(Texture* texture) noexcept
	{
		if (_textures.empty())
		{
			_textures.emplace_back(texture);
		}
		else
		{
			_textures[0] = texture;
		}
	}

	void Material::SetTextures(Texture** ppTexture, uint16 count)
	{
		if(_textures.empty())
		{
			_textures.emplace_back(ppTexture[0]);
			_textures.resize(_textures.size() + (count - 1));
			memcpy(&_textures[_textures.size() - (count - 1)], &ppTexture[0], (count - 1) * 64);
		}
		else
		{
			_textures.resize(_textures.size() + count);
			memcpy(&_textures[_textures.size() - count], &ppTexture[0], count * 64);
		}
	}

	void Material::SetTexture(Texture* pTexture) noexcept
	{
		_textures.emplace_back(pTexture);
	}

	Texture* Material::GetTexture(uint16 index)
	{
		if (_textures.size() - 1 < index)
			CE_ASSERT("ckswns", "벡터의 크기를 넘어서는 요청입니다");

		return _textures[index];
	}
}
