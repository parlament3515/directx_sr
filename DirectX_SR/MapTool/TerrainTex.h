#ifndef TerrainTex_h__
#define TerrainTex_h__

#include "GameObject.h"

class CTerrain final : public GameObject
{
public: explicit CTerrain(const DWORD& dwCntX, const DWORD& dwCntZ, const DWORD& dwIntv, const DWORD& dwIntvX);
public: virtual ~CTerrain(void);

public: virtual bool Init(void) noexcept override;
public: virtual void Update(float fElapsedTime) noexcept override;
public: virtual void Render(void) noexcept override;
public: virtual void Release(void) noexcept override;

private: DWORD						m_dwVtxCntX;
private: DWORD						m_dwVtxCntZ;
private: DWORD						m_dwVtxIntvX;
private: DWORD						m_dwVtxIntvZ;

private: DWORD						m_dwVtxCnt;
private: DWORD						m_dwVtxSize;
private: DWORD						m_dwTriCnt;
private: DWORD						m_dwFVF;

private: D3DFORMAT					m_IdxFmt;
private: DWORD						m_dwIdxSize;

private: LPDIRECT3DVERTEXBUFFER9	m_pVB;
private: LPDIRECT3DINDEXBUFFER9		m_pIB;
	   
private: LPDIRECT3DDEVICE9			m_pGraphicDev;
};

#endif // TerrainTex_h__
