#include "pch.h"
#include "TerrainTex.h"

CTerrain::CTerrain(const DWORD& dwCntX, const DWORD& dwCntZ, const DWORD& dwIntvX, const DWORD& dwIntvZ)
	: m_dwVtxCntX(dwCntX), m_dwVtxCntZ(dwCntZ), m_dwVtxIntvX(dwIntvX), m_dwVtxIntvZ(dwIntvZ)
	, m_dwVtxCnt(0),m_dwVtxSize(0),m_dwTriCnt(0), m_dwFVF(0)
	, m_dwIdxSize(0), m_IdxFmt(D3DFMT_UNKNOWN)
	, m_pIB(nullptr), m_pVB(nullptr)
{
	
}

CTerrain::~CTerrain(void)
{
	Release();
}

bool CTerrain::Init(void) noexcept
{
	m_pGraphicDev = D3D9DEVICE->GetDevice();
	m_dwFVF = VTXTEX_FVF;
	m_dwTriCnt = (m_dwVtxCntX - 1) * (m_dwVtxCntZ - 1) * 2;
	m_dwVtxCnt = m_dwVtxCntX * m_dwVtxCntZ;
	m_dwVtxSize = sizeof(VTXTEX);

	m_IdxFmt = D3DFMT_INDEX32;
	m_dwIdxSize = sizeof(INDEX32);

	if(FAILED(m_pGraphicDev->CreateVertexBuffer(m_dwVtxSize * m_dwVtxCnt, 0, m_dwFVF, D3DPOOL_MANAGED, &m_pVB, NULL)))
		return false;

	if (FAILED(m_pGraphicDev->CreateIndexBuffer(m_dwIdxSize * m_dwTriCnt, 0, m_IdxFmt, D3DPOOL_MANAGED, &m_pIB, NULL)))
		return false;
	
	VTXTEX* pVertex = nullptr;
	DWORD	dwIndex = 0;


	m_pVB->Lock(0, 0, (void**)&pVertex, 0);

	for (uint z = 0 ; z < m_dwVtxCntZ; ++z)
	{
		for (uint x = 0; x < m_dwVtxCntX; ++x)
		{
			dwIndex = z * m_dwVtxCntX + x;

			pVertex[dwIndex].vPosition = D3DXVECTOR3(float(z * m_dwVtxIntvZ), 0, float(x * m_dwVtxIntvX));
			pVertex[dwIndex].vTexUV = D3DXVECTOR2(float(z) / (m_dwVtxCntZ - 1) , float(x) / (m_dwVtxIntvX - 1));
		}
	}
	m_pVB->Unlock();
	
	DWORD		dwTriCnt = 0;
	INDEX32*	pIndex = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndex, 0);

	for (uint z = 0; z < m_dwVtxCntZ - 1; ++z)
	{
		for (uint x = 0; x < m_dwVtxCntX - 1; ++x)
		{
			dwIndex = z * m_dwVtxCntX + x;

			pIndex[dwTriCnt]._0 = dwIndex + m_dwVtxCntX;
			pIndex[dwTriCnt]._1 = dwIndex + m_dwVtxCntX + 1;
			pIndex[dwTriCnt]._2 = dwIndex + 1;
			dwTriCnt++;

			pIndex[dwTriCnt]._0 = dwIndex + m_dwVtxCntX;
			pIndex[dwTriCnt]._1 = dwIndex + 1;
			pIndex[dwTriCnt]._2 = dwIndex;
			dwTriCnt++;

		}
	}
	m_pIB->Unlock();


	return true;
}

void CTerrain::Update(float fElapsedTime) noexcept
{
}

void CTerrain::Render(void) noexcept
{
	m_pGraphicDev->SetStreamSource(0, m_pVB, 0, m_dwVtxSize);

	m_pGraphicDev->SetFVF(m_dwFVF);
	m_pGraphicDev->SetIndices(m_pIB);

	m_pGraphicDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_dwVtxCnt, 0, m_dwTriCnt);
}

void CTerrain::Release(void) noexcept
{
	if (m_pVB)
	{
		delete m_pVB;
		m_pVB = nullptr;
	}

	if (m_pIB)
	{
		delete m_pIB;
		m_pIB = nullptr;
	}
}