#pragma once
#ifndef Sturct_h__
#define Sturct_h__

typedef struct VERTEXCOLOR
{
	D3DXVECTOR3		vPos;
	DWORD		dwColor;

}VETXCOL;

const DWORD VTXCOL_FVF = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX0;

typedef struct TagVerTexTexture
{
	D3DXVECTOR3	vPosition;
	D3DXVECTOR2	vTexUV;

}VTXTEX;

const DWORD VTXTEX_FVF = D3DFVF_XYZ | D3DFVF_TEX1;

typedef struct TagVertexCubTexture
{
	D3DXVECTOR3		vPosition;
	D3DXVECTOR3		vTexUV;

}VTXCUBE;

typedef	struct TagIndex16
{
	WORD _0;
	WORD _1;
	WORD _2;

}INDEX16;

typedef	struct TagIndex32
{
	DWORD _0;
	DWORD _1;
	DWORD _2;

}INDEX32;


#endif // Sturct_h__