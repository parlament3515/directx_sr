﻿#pragma once

class ParticleToolDoc : public CDocument
{

protected:						ParticleToolDoc();
		DECLARE_DYNCREATE(ParticleToolDoc)

public:		virtual BOOL		OnNewDocument();
public:		virtual void			Serialize(CArchive& ar);   // 문서 입/출력을 위해 재정의되었습니다.
#ifdef SHARED_HANDLERS
	  virtual void InitializeSearchContent();
	  virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

public:		virtual			~ParticleToolDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif




protected:	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// 검색 처리기에 대한 검색 콘텐츠를 설정하는 도우미 함수
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};
