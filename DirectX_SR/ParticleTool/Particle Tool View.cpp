﻿// ParticleToolView.cpp: 구현 파일
//

#include "pch.h"
#include "ParticleTool.h"
#include "Particle Tool View.h"


// ParticleToolView

IMPLEMENT_DYNCREATE(ParticleToolView, CView)

ParticleToolView::ParticleToolView()
{

}

ParticleToolView::~ParticleToolView()
{
}

BEGIN_MESSAGE_MAP(ParticleToolView, CView)
END_MESSAGE_MAP()


// ParticleToolView 그리기

void ParticleToolView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();

	D3D9DEVICE->Begin();
	_pParticle->Render();

	D3D9DEVICE->End();
}


// ParticleToolView 진단

#ifdef _DEBUG
void ParticleToolView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void ParticleToolView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// ParticleToolView 메시지 처리기
void ParticleToolView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	if(FAILED(D3D9DEVICE->Init(m_hWnd,400,800, D3DXCOLOR(1.f, 1.f, 1.f, 1.f))))
		AfxMessageBox(L"CDevice Create Fail");

	_pParticle = new Particle();
	_pParticle->Init();
}

void ParticleToolView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(_pParticle)
		_pParticle->Update(TIMEMANAGER->GetDeltaTime());
}
