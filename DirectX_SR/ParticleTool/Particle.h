#pragma once
#ifndef __PARTICLE_H__
#define __PARTICLE_H__

#include "GameObject.h"
#include "Texture.h"

class Particle : public GameObject
{
public:		explicit							Particle();
public:		virtual							~Particle();

public:		virtual bool						Init(void) noexcept override;
public:		virtual void						Update(float fElapsedTime) noexcept override;
public:		virtual void						Render(void) noexcept override;
public:		virtual void						Release(void) noexcept override;

private:		void								Reset();
private:		void								ResetParticle(PARTICLEATTRIBUTE* attribute);
private:		void								AddParticle();

private:		LPDIRECT3DDEVICE9						_pGraphicDev;
private:		std::list<PARTICLEATTRIBUTE*>	_tParticles;
private:		PARTICLEATTRIBUTE					_tOrign;

private:		float											_fSize;

private:		Texture*										_pTexture;
private:		IDirect3DVertexBuffer9*				_pVB;

private:		DWORD										_dwVbSize;
private:		DWORD										_dwVbOffset;
private:		DWORD										_dwVbBathSize;


};

#endif // !__PARTICLE_H__