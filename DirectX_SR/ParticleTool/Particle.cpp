#include "pch.h"
#include "Particle.h"

Particle::Particle()
{
}

Particle::~Particle()
{
	Release();
}

bool Particle::Init(void) noexcept
{
    _pGraphicDev = D3D9DEVICE->GetDevice();

    if(FAILED(_pGraphicDev->CreateVertexBuffer(_dwVbSize * sizeof(PARTICLE),
        D3DUSAGE_DYNAMIC | D3DUSAGE_POINTS | D3DUSAGE_WRITEONLY,
        FVF_PARTICLE,
        D3DPOOL_DEFAULT,
        &_pVB, 0)))
        return false;
        
    _dwVbSize = 1024;
    _dwVbOffset = 0;
    _dwVbBathSize = 256;

	//_pTexture->Init(_pGraphicDev, "");

    return true;
}

void Particle::Update(float fElapsedTime) noexcept
{
    std::list<PARTICLEATTRIBUTE*>::iterator iter;
    for (iter = _tParticles.begin(); iter != _tParticles.end(); ++iter)
    {
        if ((*iter)->_bIsAlive)
        {
            (*iter)->_vPosition += (*iter)->_vVelocity * fElapsedTime;

            (*iter)->_fAge += fElapsedTime;
            if( (*iter)->_fAge >= (*iter)->_fLifeTime)
            {
                if ((*iter)->_bLoop)
                    ResetParticle(*iter);
                else
                    (*iter)->_bIsAlive = false;
            }

        }
    }
}

void Particle::Render(void) noexcept
{
	if (_tParticles.empty()) return;

	_pGraphicDev->SetRenderState(D3DRS_POINTSPRITEENABLE, true);
	_pGraphicDev->SetRenderState(D3DRS_POINTSCALEENABLE, true);
	_pGraphicDev->SetRenderState(D3DRS_POINTSIZE, FtoDW(_fSize));
	_pGraphicDev->SetRenderState(D3DRS_POINTSIZE_MIN, FtoDW(0.0f));

	// 거리에따른 사이즈 조절
	_pGraphicDev->SetRenderState(D3DRS_POINTSCALE_A, FtoDW(0.0f));
	_pGraphicDev->SetRenderState(D3DRS_POINTSCALE_B, FtoDW(0.0f));
	_pGraphicDev->SetRenderState(D3DRS_POINTSCALE_C, FtoDW(1.0f));

	//알파 값 사용
	_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	_pGraphicDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);

	_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	_pGraphicDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	_pGraphicDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//	_tParticles->SetTransform(D3DTS_WORLD, _pTransformCom->Get_WorldMatrixPointer());
	//	_tParticles->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	

	_pGraphicDev->SetFVF(FVF_PARTICLE);
	_pGraphicDev->SetStreamSource(0, _pVB, 0, sizeof(PARTICLE));

	if (_dwVbOffset >= _dwVbSize)
		_dwVbOffset = 0;

	PARTICLE* v = 0;
	_pVB->Lock(_dwVbOffset * sizeof(PARTICLE),
		_dwVbBathSize * sizeof(PARTICLE),
		(void**)&v,
		_dwVbOffset ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);

	DWORD numParticlesInBatch = 0;

	std::list<PARTICLEATTRIBUTE*>::iterator iter;
	for (iter = _tParticles.begin(); iter != _tParticles.end(); iter++)
	{
		if ((*iter)->_bIsAlive)
		{
			v->vPosition = (*iter)->_vPosition;
			v->dwColor = (D3DCOLOR)(*iter)->_dwColor;
			v->fSize = 1.f;
			v++;
			numParticlesInBatch++;

			if (numParticlesInBatch == _dwVbBathSize)
			{
				_pVB->Unlock();

				_pGraphicDev->DrawPrimitive(D3DPT_POINTLIST, _dwVbOffset, _dwVbBathSize);

				_dwVbOffset += _dwVbBathSize;
				if (_dwVbOffset >= _dwVbSize)
					_dwVbOffset = 0;

				_pVB->Lock(_dwVbOffset * sizeof(PARTICLE),
					_dwVbBathSize * sizeof(PARTICLE),
					(void**)&v,
					_dwVbOffset ? D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);

				numParticlesInBatch = 0;
			}
		}
	}
	_pVB->Unlock();

	if (numParticlesInBatch)
	{
		_pGraphicDev->DrawPrimitive(D3DPT_POINTLIST, _dwVbOffset, numParticlesInBatch);
	}
	_dwVbOffset += _dwVbBathSize;

	//	설정 되돌리기
	_pGraphicDev->SetRenderState(D3DRS_POINTSPRITEENABLE, false);
	_pGraphicDev->SetRenderState(D3DRS_POINTSCALEENABLE, false);
	_pGraphicDev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);

}

void Particle::Release(void) noexcept
{
    if (_pVB)
    {
        delete _pVB;
        _pVB = nullptr;
    }

    if (!_tParticles.empty())
    {
        for (auto& iter = _tParticles.begin(); iter != _tParticles.end();)
            Safe_Delete(iter);

        _tParticles.clear();
    }

}

void Particle::Reset()
{
   std::list<PARTICLEATTRIBUTE*>::iterator iter;
    for (iter = _tParticles.begin(); iter != _tParticles.end(); iter++)
        ResetParticle(*iter);
}

void Particle::ResetParticle(PARTICLEATTRIBUTE* attribute)
{
    *attribute = _tOrign; 
}

void Particle::AddParticle()
{
    PARTICLEATTRIBUTE* attribute = nullptr;
    ResetParticle(attribute);

    _tParticles.push_back(attribute);
}
