#pragma once
#ifndef __STRUCT_H__
#define __STRUCT_H__

typedef struct tagParticle
{
	D3DXVECTOR3	 vPosition;
	unsigned long	 dwColor;
	float	 fSize;
}PARTICLE;
const unsigned long FVF_PARTICLE = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_PSIZE;

typedef struct tagParticleAttribute
{
	//tagParticleAttribute()
	//{
	//	_vPosition = Vector3::zero;
	//	_vVelocity = Vector3::zero;
	//	_acceleration = Vector3::zero;

	//	_fLifeTime = 0.0f;
	//	_fAge = 0.0f;
	//	_dwColor = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
	//	_bIsAlive = true;
	//	_bLoop = true;
	//}

	Vector3			_vPosition;
	Vector3			_vVelocity;
	Vector3			_acceleration;
	float				_fLifeTime;     
	float				_fAge;          
	DWORD			_dwColor;       
	bool				_bIsAlive;
	bool				_bLoop;

}PARTICLEATTRIBUTE;


#endif // !__STRUCT_H__
