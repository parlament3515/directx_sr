﻿#pragma once

// ParticleToolView 보기
#include "Particle.h"

class ParticleToolDoc;
class ParticleToolView : public CView
{
	DECLARE_DYNCREATE(ParticleToolView)

protected:	ParticleToolView();           // 동적 만들기에 사용되는 protected 생성자입니다.
protected:	virtual ~ParticleToolView();

public:		 virtual void		 OnDraw(CDC* pDC);      // 이 뷰를 그리기 위해 재정의되었습니다.

#ifdef _DEBUG
	  virtual void AssertValid() const;
#ifndef _WIN32_WCE
	  virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:	DECLARE_MESSAGE_MAP()
public:		virtual void				OnInitialUpdate();

private:		Particle*					_pParticle;
	   virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
};


#ifndef _DEBUG  // MFC ToolView.cpp의 디버그 버전
inline ParticleToolDoc* CMFCToolView::GetDocument() const
{
	return reinterpret_cast<CMFCToolDoc*>(m_pDocument);
}
#endif


