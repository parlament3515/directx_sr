#pragma once
#ifndef __FUNCTION_H__
#define __FUNCTION_H__

template<typename T>
void	Safe_Delete(T& Pointer)
{
	if (nullptr != *Pointer)
	{
		delete *Pointer;
		*Pointer = nullptr;
	}
}

inline DWORD FtoDW(float f) { return *((DWORD*)&f); }

#endif //__FUNCTION_H__