﻿
// MainFrm.h: CMainFrame 클래스의 인터페이스
//

#pragma once

class CMainFrame : public CFrameWnd
{
	
public:			CMainFrame() noexcept;
public:			virtual ~CMainFrame();
protected:		DECLARE_DYNAMIC(CMainFrame)

// 재정의입니다.
public:			virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:		CSplitterWnd _MainSplitter;

// 생성된 메시지 맵 함수
protected:		afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:		DECLARE_MESSAGE_MAP()

protected:		virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
};


